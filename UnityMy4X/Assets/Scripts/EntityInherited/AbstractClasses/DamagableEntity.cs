﻿using UnityEngine;

/// <summary>
/// Superclass for all entities that can receive damage/healing and die.
/// </summary>
public abstract class DamagableEntity : Entity, IDamagable
{
	//Current health of the entity. Can not exceed the max health.
	[SerializeField]
	protected float _health;
	//Maximum health the entity can have. Can be modified during the game.
	[SerializeField]
	protected float _maxHealth;

	/// <summary>
	/// Property for _health
	/// </summary>
	public virtual float Health {
		get
		{
			return _health;
		}

		set
		{
			_health = value;
		}
	}

	/// <summary>
	/// Property for _maxHealth
	/// </summary>
	public virtual float MaxHealth {
		get
		{
			return _maxHealth;
		}

		set
		{
			_maxHealth = value;
		}
	}

	/// <summary>
	/// Lowers the health by the received damage. Does nothing if damage is negative.
	/// </summary>
	/// <param name="damage">
	/// Damage that is dealth to this entity
	/// </param>
	/// <returns>
	/// Returns true if health is reduced to zero by the damage or entity was already dead.
	/// </returns>
	public virtual bool ReceiveDamage(float damage)
	{
		if (damage > 0)
		{
			if (damage < _health)
			{
				_health -= damage;
			}
			else
			{
				_health = 0;
				Die();
				return true;
			}
		}

		//Returns true if entity has been killed by anything else this frame before this method was executed.
		//TODO: This does not return true if the internal execution order "kills" the entity later.
		if (_isDead == true)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/// <summary>
	/// Adds healing to health if not exceeding max health. if healing is negative does not add anything. 
	/// </summary>
	/// <param name="healing">
	/// Positive amount of health that wishes to be add to current health of the entity.
	/// </param>
	/// <returns>
	/// Returns true if either healing was enough to set health to max health or if max health was already established.
	/// </returns>
	public virtual bool ReceiveHealing(float healing)
	{
		if (healing > 0)
		{
			float maxHealing = _maxHealth - _health;

			if ( healing < maxHealing)
			{
				_health += healing;
			}
			else
			{
				_health = _maxHealth;
				return true;
			}
		}

		if (_health == _maxHealth)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
