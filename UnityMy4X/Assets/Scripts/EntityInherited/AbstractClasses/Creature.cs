﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Superclass for movable, damagable entities.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public abstract class Creature : DamagableEntity, IMovable
{
	//the movement speed as a multiplier on the velocity
	[SerializeField]
	protected float _movementSpeed = 1;

	//the rigidbody of this game object for the movement methods
	protected Rigidbody _rb;

	/// <summary>
	/// Field to provice info about collision.
	/// </summary>
	public bool _isColliding = false;

	public float MovementSpeed {
		get
		{
			return _movementSpeed;
		}

		set
		{
			_movementSpeed = value;
		}
	}

	//TODO: Think about this. Is this really good?
	public void OnCollisionEnter(Collision collision)
	{
		_isColliding = true;
		StartCoroutine(ResetCollisionBool());
	}

	private IEnumerator ResetCollisionBool()
	{
		yield return new WaitForEndOfFrame();
		_isColliding = false;
	}

	/// <summary>
	/// Adds an impulse to the rigidbody based on its mass in the given local direction.
	/// </summary>
	/// <param name="localDirection">
	/// The given local direction. The length of the vector has no influence on this method.
	/// </param>
	public virtual void KnockBack(Vector3 localDirection)
	{
		float forceMultiplier = _rb.mass;

		Vector3 globalDirection = transform.TransformDirection(localDirection.normalized);

		_rb.AddForce(forceMultiplier * globalDirection, ForceMode.Impulse);
	}

	/// <summary>
	/// Moves the entity in the direction of the destination. Height does not matter.
	/// </summary>
	/// <param name="destination">
	/// Any position in the game world. 
	/// </param>
	public virtual void MoveByDestination(Vector3 destination)
	{
		//normalized direction from destination without height
		Vector3 nDirection = destination - transform.position;
		nDirection.y = 0;
		nDirection = nDirection.normalized;

		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(nDirection), 0.125f);
		_rb.velocity = nDirection;
		
	}

	/// <summary>
	/// Moves the entity in the given direction. Height does not matter.
	/// </summary>
	/// <param name="direction">
	/// Any Vector3. Length has no influence on the movement speed.
	/// direction is in global coordinates.
	/// </param>
	public virtual void MoveInDirection(Vector3 direction)
	{
		Vector3 nDirection = direction;
		nDirection.y = 0;
		nDirection = nDirection.normalized;

		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(nDirection),0.125f); 
		_rb.velocity = nDirection;
	}


	/// <summary>
	/// Moves the entity in the given direction. Height does not matter.
	/// </summary>
	/// <param name="direction">
	/// Any Vector2. Length has no influence on the movement speed.
	/// direction is in local coordinates. x in 2D is x in 3D. y in 2D is z in 3D.
	/// </param>
	public virtual void MoveInDirection(Vector2 localDirection)
	{
		Vector3 nDirection = transform.TransformDirection(new Vector3(localDirection.x, 0, localDirection.y));
		nDirection = nDirection.normalized;

		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(nDirection), 0.125f);
		_rb.velocity = nDirection;
	}

	/// <summary>
	/// Stops the entity.
	/// </summary>
	public virtual void Stop()
	{
		_rb.velocity = new Vector3();
	}
}
