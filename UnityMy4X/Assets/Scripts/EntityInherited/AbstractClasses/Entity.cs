﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Superclass for all game objects that can die.
/// </summary>
public abstract class Entity : MonoBehaviour
{
	protected bool _isDead = false;

	public bool IsDead { get { return _isDead; } }

	/// <summary>
	/// Sets the death boolean to true and removes the game object at the end of the frame, i.e. all interactions are still happening in the frame a entity is dying.
	/// </summary>
	public virtual void Die()
	{
		_isDead = true;
		DestroyImmediate(gameObject); //TODO: Understand why WaitUntilEndOfFrame didnt work here.
	}
}
