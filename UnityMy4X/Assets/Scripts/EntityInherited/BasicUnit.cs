using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicUnit : Creature, IFighter
{
	[SerializeField]
	protected float _attackRange;
	[SerializeField]
	protected float _engageRange;
	[SerializeField]
	protected float _attackDamage;
	[SerializeField]
	protected float _attackCooldown;


	public float AttackRange {
		get {return _attackRange;}
		set { }
	}

	public float EngageRange {
		get {return _engageRange;}
		set { }
	}

	public float AttackCooldown {
		get {return _attackCooldown;}
		set { }
	}

	void Awake()
	{
		_rb = GetComponent<Rigidbody>();
	}

	public void AttackTarget(DamagableEntity target)
	{
		target.ReceiveDamage(_attackDamage);
	}

	/// <summary>
	/// Uses an overlap sphere on the layer FactionDEntities to get all damagable entities in range.
	/// </summary>
	/// <param name="range">
	/// Size of the overlap sphere.
	/// </param>
	/// <param name = "engageRangeCap" >
	/// if true the range cant be higher than the engage range.
	/// </param>
	/// <returns>
	/// Array of damagable entities in specified range.
	/// </returns>
	public DamagableEntity[] TargetsInSpecificRange(float range, bool engageRangeCap = false)
	{
		int enemyLayer = 1 << LayerMask.NameToLayer("FactionDEntities");

		if( engageRangeCap == true)
		{
			if (range > _engageRange)
			{
				range = _engageRange;
			}

		}

		Collider[] overlapResult = Physics.OverlapSphere(gameObject.transform.position, range, enemyLayer);

		DamagableEntity[] dEntitiesInRange = new DamagableEntity[overlapResult.Length];

		for (int i = 0; i < overlapResult.Length; i++)
		{
			DamagableEntity _tempDEntity = overlapResult[i].GetComponent<DamagableEntity>();
			if (_tempDEntity != null)
			{
				dEntitiesInRange[i] = _tempDEntity;
			}
			else
			{
				Debug.Log("Enemy in range without being damagable!");
			}

		}

		return dEntitiesInRange;
	}


	/// <summary>
	/// Uses an overlap sphere on the layer FactionDEntities to get all damagable entities in engage range.
	/// </summary>
	/// <returns>
	/// Array of damagable entities in engage range.
	/// </returns>
	public DamagableEntity[] TargetsInEngageRange()
	{
		int enemyLayer = 1 << LayerMask.NameToLayer("FactionDEntities");

		Collider[] overlapResult = Physics.OverlapSphere(gameObject.transform.position, _engageRange, enemyLayer);

		DamagableEntity[] dEntitiesInRange = new DamagableEntity[overlapResult.Length];

		for (int i = 0; i < overlapResult.Length; i++)
		{
			DamagableEntity _tempDEntity = overlapResult[i].GetComponent<DamagableEntity>();
			if (_tempDEntity != null)
			{
				dEntitiesInRange[i] = _tempDEntity;
			}
			else
			{
				Debug.Log("Enemy in range without being damagable!");
			}
			
		}

		return dEntitiesInRange;
	}

	/// <summary>
	/// Checks if the target position has a smaller (or equal) distance to the position of this game object than the attack range.
	/// </summary>
	/// <param name="target"></param>
	/// <returns></returns>
	public bool TargetIsInAttackRange(DamagableEntity target)
	{
		if (target != null)
		{
			if ((target.transform.position - transform.position).sqrMagnitude <= _attackRange * _attackRange)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	
}
