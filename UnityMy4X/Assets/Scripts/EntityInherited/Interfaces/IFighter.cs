﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFighter
{
	float AttackRange { get; set; }
	float EngageRange { get; set; }
	float AttackCooldown { get; set; }

	DamagableEntity[] TargetsInEngageRange();

	bool TargetIsInAttackRange(DamagableEntity target);

	DamagableEntity[] TargetsInSpecificRange(float range, bool engageRangeCap = false);

	void AttackTarget(DamagableEntity target);
}
