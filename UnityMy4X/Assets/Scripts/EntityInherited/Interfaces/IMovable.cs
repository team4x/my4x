﻿using UnityEngine;

//TODO: Do i need acceleration?
/// <summary>
/// Interface for very basic movement.
/// </summary>
public interface IMovable
{
	/// <summary>
	/// Movement speed of this entity.
	/// </summary>
	float MovementSpeed { get; set; }

	/// <summary>
	/// Moves the entity in the direction of the destination. Height does not matter.
	/// </summary>
	/// <param name="destination">
	/// Any position in the game world. 
	/// </param>
	void MoveByDestination(Vector3 destination);

	/// <summary>
	/// Moves the entity in the given direction. Height does not matter.
	/// </summary>
	/// <param name="direction">
	/// Any Vector3. Length has no influence on the movement speed.
	/// direction is in global coordinates.
	/// </param>
	void MoveInDirection(Vector3 direction);

	/// <summary>
	/// Moves the entity in the given direction. Height does not matter.
	/// </summary>
	/// <param name="direction">
	/// Any Vector2. Length has no influence on the movement speed.
	/// direction is in local coordinates. x in 2D is x in 3D. y in 2D is z in 3D.
	/// </param>
	void MoveInDirection(Vector2 localDirection);

	/// <summary>
	/// Entity gets a short impuls in the given direction.
	/// </summary>
	/// <param name="localDirection">
	/// Direction of the knock back in local space.
	/// </param>
	void KnockBack(Vector3 localDirection);

	/// <summary>
	/// Stops the entity.
	/// </summary>
	void Stop();
}
