﻿
/// <summary>
/// Interface for everything that can get damaged and reveice healing.
/// </summary>
public interface IDamagable
{
	/// <summary>
	/// Property for the current health of the entity.
	/// </summary>
	float Health { get; set; }

	/// <summary>
	/// Property for the max health of the entity. This value can be changed in game.
	/// </summary>
	float MaxHealth { get; set; }

	/// <summary>
	/// For all incoming damage.
	/// </summary>
	/// <param name="damage"></param>
	/// <returns>
	/// Returns true if received damage killed the entity or entity was killed by anything else before the execution of this method in this frame.
	/// </returns>
	bool ReceiveDamage(float damage);

	/// <summary>
	/// Healing cant give you more health than max Health for unit.
	/// </summary>
	/// <param name="healing"></param>
	/// <returns>
	/// Returns true if entity is fully healed after receiving healing.
	/// </returns>
	bool ReceiveHealing(float healing);
}
