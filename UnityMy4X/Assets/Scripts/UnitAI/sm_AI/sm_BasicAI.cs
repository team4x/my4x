﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sm_BasicAI : sm_State
{
	/// <summary>
	/// the unit being controlled by the ai state machine
	/// </summary>
	protected BasicUnit _thisUnit;

	/// <summary>
	/// target of the unit. has lower priority than the destination.
	/// </summary>
	protected Entity _target = null;

	/// <summary>
	/// destination of the unit. has higher priority than the target.
	/// </summary>
	protected Destination _destination = null;

	protected sm_State _transitionState;

	public sm_BasicAI(sm_StateManager stateManager, sm_State superState, sm_State subState, sm_State lastState, BasicUnit thisUnit) : base(stateManager, superState, subState, lastState)
	{
		_thisUnit = thisUnit;
		_currentSubState = new sm_MoveStates(null,this,null,null, _thisUnit,_target,_destination, 5f);
		_transitionState = _currentSubState;
	}

	/// <summary>
	/// currently no internal stuff going on in this state.
	/// </summary>
	protected override void StateActivity(){}

	/// <summary>
	/// The NEW_TARGET event is now a transition out of this state into the substate move states to have the move states reevaluate the correct substate by entry.
	/// </summary>
	/// <returns></returns>
	protected override sm_State Transitions()
	{
		Entity _newTarget;
		if (NewTargetInRange(out _newTarget))
		{
			_target = _newTarget;
			((sm_MoveStates)_transitionState)._target = _target;
			return _transitionState;
		}

		return null;
	}
	
	//TODO: Build priorities into the game.
	/// <summary>
	/// Finds the target closest to this unit.
	/// </summary>
	/// <param name="targetWithHighestPriority">
	/// gives the closest target. returns null if there is none.
	/// </param>
	/// <returns>
	/// returns true if closest target is not the current target.
	/// </returns>
	private bool NewTargetInRange(out Entity targetWithHighestPriority)
	{
		//we start like this
		targetWithHighestPriority = null;

		//if there is no target get the closes target
		if (_target == null)
		{
			Entity[] entityArray = _thisUnit.TargetsInEngageRange();

			float minSqrDistance = Mathf.Sqrt(float.MaxValue);

			for (int i = 0; i < entityArray.Length; i++)
			{
				float currentSqrDistance = (entityArray[i].transform.position - _thisUnit.transform.position).sqrMagnitude;

				if (currentSqrDistance < minSqrDistance)
				{
					targetWithHighestPriority = entityArray[i];
					minSqrDistance = currentSqrDistance;
				}
			}

		}
		//Do everything only up to the distance of the current target.
		else
		{
			float range = (_target.transform.position - _thisUnit.transform.position).magnitude;

			Entity[] entityArray = _thisUnit.TargetsInSpecificRange(range, true);

			float minSqrDistance = Mathf.Sqrt(float.MaxValue);

			for (int i = 0; i < entityArray.Length; i++)
			{
				float currentSqrDistance = (entityArray[i].transform.position - _thisUnit.transform.position).sqrMagnitude;

				if (currentSqrDistance < minSqrDistance)
				{
					targetWithHighestPriority = entityArray[i];
					minSqrDistance = currentSqrDistance;
				}
			}
		}
		
		//evaluate if target is new
		if (_target != targetWithHighestPriority)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
