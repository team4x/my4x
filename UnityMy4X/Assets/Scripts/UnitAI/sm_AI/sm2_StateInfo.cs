﻿using System.Collections.Generic;

public class sm2_StateInfo
{
	public sm2_State _stateReference;

	public bool _stateIsActive = false; //TODO: Needed?

	public List<object> _stateData;
	
	sm2_StateInfo(sm2_State state)
	{
		_stateReference = state;

		_stateData = new List<object>();
	}																														

}