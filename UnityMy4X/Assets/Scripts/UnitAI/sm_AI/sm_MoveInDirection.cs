﻿using UnityEngine;

public class sm_MoveInDirection : sm_State
{
	/// <summary>
	/// the unit being controlled by the ai state machine
	/// </summary>
	protected BasicUnit _thisUnit;

	protected sm_State _transitionState;

	/// <summary>
	/// The direction the unit is moving to. Can be transmitted by another state. Does not change in this state.
	/// </summary>
	public Vector3 _direction;

	public sm_MoveInDirection(sm_StateManager stateManager, sm_State superState, sm_State subState, sm_State lastState, BasicUnit thisUnit, Vector3 direction) : base(stateManager, superState, subState, lastState)
	{
		_thisUnit = thisUnit;
		_direction = direction;
	}

	/// <summary>
	/// depends on the superstate.
	/// </summary>
	protected override void StateActivity()
	{
		_thisUnit.MoveInDirection(_direction);
	}

	protected override sm_State Transitions()
	{
		if (_thisUnit._isColliding == true)
		{
			if (_transitionState != null)
			{
				return _transitionState;
			}
			else
			{
				return null; //_transitionState = sm_AvoidCollision
			}
		}
		else
		{
			return null;
		}
	}
}
