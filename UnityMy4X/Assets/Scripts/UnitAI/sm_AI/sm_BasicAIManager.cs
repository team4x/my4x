﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BasicUnit))]
public class sm_BasicAIManager : sm_StateManager
{
	private void Awake()
	{
		sm_BasicAI entryState = new sm_BasicAI(this, null, null, null, GetComponent<BasicUnit>());

		_currentStateUpdate = entryState.sm_StateUpdate;
	}
}
