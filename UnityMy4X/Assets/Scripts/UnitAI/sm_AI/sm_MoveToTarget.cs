﻿public class sm_MoveToTarget : sm_State
{
	/// <summary>
	/// the unit being controlled by the ai state machine
	/// </summary>
	protected BasicUnit _thisUnit;

	/// <summary>
	/// target of the unit. has lower priority than the destination.
	/// </summary>
	public Entity _target = null;

	protected sm_State _transitionState;

	public sm_MoveToTarget(sm_StateManager stateManager, sm_State superState, sm_State subState, sm_State lastState, BasicUnit thisUnit, Entity target) : base(stateManager, superState, subState, lastState)
	{
		_thisUnit = thisUnit;
		_target = target;
	}

	protected override void StateActivity()
	{
		if (_target != null)
		{
			_thisUnit.MoveByDestination(_target.gameObject.transform.position);
		}
	}

	protected override sm_State Transitions()
	{
		if (_thisUnit._isColliding == true)
		{
			if (_transitionState != null)
			{
				return _transitionState;
			}
			else
			{
				return null; //_transitionState = sm_AvoidCollision
			}
		}
		else
		{
			return null;
		}
	}
}
