using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sm_MoveStates : sm_State
{
	/// <summary>
	/// the unit being controlled by the ai state machine
	/// </summary>
	protected BasicUnit _thisUnit;

	/// <summary>
	/// target of the unit. has lower priority than the destination.
	/// </summary>
	public Entity _target = null;

	/// <summary>
	/// destination of the unit. has higher priority than the target.
	/// </summary>
	protected Destination _destination = null;

	/// <summary>
	/// direction of the movement of the unit. has the lowest priority.
	/// </summary>
	protected Vector3 _direction = Vector3.forward;

	/// <summary>
	/// field to remember the last time the direction was changed/set.
	/// </summary>
	protected float _lastDirectionChange = Time.time;

	protected float _directionHoldTime = 5; //TODO: think about a possibility to change direction hold time and deviation of the direction in unity editor.

	/// <summary>
	/// one of the entry sub states.
	/// </summary>
	protected sm_State _subStateDestination;

	/// <summary>
	/// one of the entry sub states.
	/// </summary>
	protected sm_State _subStateTarget;

	/// <summary>
	/// one of the entry sub states.
	/// </summary>
	protected sm_State _subStateDirection;

	protected sm_State _transitionStateTargetHandling;

	public sm_MoveStates(sm_StateManager stateManager, sm_State superState, sm_State subState, sm_State lastState, BasicUnit thisUnit, Entity target, Destination destination, float directionHoldTime) : base(stateManager, superState, subState, lastState)
	{
		_thisUnit = thisUnit;
		_target = target;
		_destination = destination;
		

		InitializeDirection();

		_directionHoldTime = directionHoldTime;
	}

	
	protected override void StateActivity()
	{
		if (UpdateDirection())
		{
			if (_subStateDirection != null)
			{
				((sm_MoveInDirection)_subStateDirection)._direction = _direction;
			}
		}
	}

	protected override sm_State Transitions()
	{
		if (TargetReached())
		{
			if (_transitionStateTargetHandling != null)
			{
				((sm_TargetHandling)_transitionStateTargetHandling)._target = _target;
				return _transitionStateTargetHandling;
			}
			else
			{
				_transitionStateTargetHandling = new sm_TargetHandling(null, _superState, null, this, _thisUnit, _target);
				return _transitionStateTargetHandling;
			}
		}

		//If no transition happens.
		return null;
	}

	//TODO: this will have to be extended to collectibles
	private bool TargetReached()
	{
		if (_target != null)
		{
			if (_target is DamagableEntity)
			{
				if (_thisUnit.TargetIsInAttackRange((DamagableEntity)_target))
				{
					return true;
				}
			}
			else if (_target is BasicCollectible)
			{
				return false;
			}
		}

		return false;
	}

	protected override void Entry()
	{
		base.Entry();
		ChooseEntrySubState();
	}

	/// <summary>
	/// Sets direction to a random direction and resets the direction change time.
	/// </summary>
	private void InitializeDirection()
	{
		float deviation = 2 * Mathf.PI;
		_direction = GetRandomDirection(deviation);
		_lastDirectionChange = Time.time;
	}

	/// <summary>
	/// Changes the direction with a deviation of pi after the direction hold time has elapsed.
	/// </summary>
	private bool UpdateDirection()
	{
		if ((Time.time - _lastDirectionChange) > _directionHoldTime)
		{
			float deviation = Mathf.PI;
			_direction = GetRandomDirection(deviation);
			_lastDirectionChange = Time.time;
			return true;
		}
		else
		{
			return false;
		}
	}

	/// <summary>
	/// Returns a randomized direction with a deviation depending on the input of the current direction.
	/// </summary>
	/// <param name="deviation"></param>
	/// <returns></returns>
	private Vector3 GetRandomDirection(float deviation)
	{
		float rad = UnityEngine.Random.Range(-deviation*0.5f,deviation * 0.5f);

		Vector3 newDirection = _thisUnit.transform.TransformDirection(new Vector3(Mathf.Sin(rad), 0, Mathf.Cos(rad)));

		return newDirection;
	}

	/// <summary>
	/// sets the global substate field to the correct default value.
	/// </summary>
	private void ChooseEntrySubState()
	{
		if (_destination != null)
		{
			_currentSubState = null; //TODO: replace null with sm_MoveToDestination
		}
		else
		{
			if (_target != null)
			{
				if (_subStateTarget != null)
				{
					((sm_MoveToTarget)_subStateTarget)._target = _target;
					_currentSubState = _subStateTarget; 
				}
				else
				{
					_subStateTarget = new sm_MoveToTarget(null,this,null,null,_thisUnit,_target);
					_currentSubState = _subStateTarget;
				}
			}
			else
			{
				if (_subStateDirection != null)
				{
					((sm_MoveInDirection)_subStateDirection)._direction = _direction;
					_currentSubState = _subStateDirection; 
				}
				else
				{
					_subStateDirection = new sm_MoveInDirection(null, this, null, this, _thisUnit, _direction);
					_currentSubState = _subStateDirection;
				}
			}
		}
	}
}
