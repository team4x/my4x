﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// To the game object attached class to use a AI state machine.
/// </summary>
public class sm2_UnitComponent : MonoBehaviour
{
	[SerializeField]
	protected sm2_StateMachineDefiner _stateMachineDefiner;

	public delegate void sm2_Update(sm2_UnitComponent unitComponent);

	public sm2_Update _currentTopLevelStateUpdate;

	/// <summary>
	/// variable storage for the states of the state machine
	/// </summary>
	List<sm2_StateInfo> _infoList = new List<sm2_StateInfo>();

	private void Awake()
	{
		_currentTopLevelStateUpdate = _stateMachineDefiner._entryState.sm2_StateUpdate;
	}

	void Update()
	{
		if (_currentTopLevelStateUpdate != null)
		{
			_currentTopLevelStateUpdate(this);
		}
	}

	
}
