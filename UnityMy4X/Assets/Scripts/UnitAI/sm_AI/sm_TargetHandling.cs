using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sm_TargetHandling : sm_State
{
	/// <summary>
	/// The unit beeing controlled by this state machine
	/// </summary>
	BasicUnit _thisUnit = null;

	/// <summary>
	/// The target to move to. Overrides directions.
	/// </summary>
	public Entity _target = null;

	/// <summary>
	/// A field to safe the last time this state updated itself
	/// </summary>
	float _attack_lastPointInTime = Time.time;

	/// <summary>
	/// Cooldown of the basic attack
	/// </summary>
	float _attackCooldown;


	sm_State _transitionStateMoveStates;

	public sm_TargetHandling(sm_StateManager stateManager, sm_State superState, sm_State subState, sm_State lastState, BasicUnit thisUnit, Entity target) : base(stateManager, superState, subState, lastState)
	{
		_thisUnit = thisUnit;
		_target = target;

		//attack cool down time is initialized in the unit
		_attackCooldown = _thisUnit.AttackCooldown;

		//this state is always created by movestates and will set movestates to its transition state
		_transitionStateMoveStates = _lastState;
	}

	protected override void StateActivity()
	{
		if (AttackNotOnCooldown())
		{
			if (_target != null)
			{
				_thisUnit.AttackTarget((DamagableEntity)_target);
			}
		}
	}

	private bool AttackNotOnCooldown()
	{
		if ((Time.time - _attack_lastPointInTime) >= _attackCooldown)
		{
			_attack_lastPointInTime = Time.time + ((Time.time - _attack_lastPointInTime) - _attackCooldown);
			return true;
		}

		return false;
	}

	protected override sm_State Transitions()
	{
		if (TargetNotInAttackRange())
		{
			if (_transitionStateMoveStates != null)
			{
				return _transitionStateMoveStates;
			}
			else
			{
				throw new Exception("State target handling cant be created before state move states.");
			}
		}
		return null;
	}

	private bool TargetNotInAttackRange()
	{
		if ( _target is DamagableEntity)
		{
			if (_thisUnit.TargetIsInAttackRange((DamagableEntity)_target))
			{
				return false;
			}
		}

		return true;
		
	}

	protected override void Entry()
	{
		base.Entry();
		_thisUnit.Stop();
		_attack_lastPointInTime = Time.time - _attackCooldown;
	}
}
