﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// State manager class connecting state machine states with unity. The method delegate in the update of this class is changed by the states.
/// </summary>
public abstract class sm_StateManager : MonoBehaviour
{
	public delegate void sm_Update();

	public sm_Update _currentStateUpdate;

	void Update ()
	{
		if (_currentStateUpdate != null)
		{
			_currentStateUpdate();
		}
	}
}
