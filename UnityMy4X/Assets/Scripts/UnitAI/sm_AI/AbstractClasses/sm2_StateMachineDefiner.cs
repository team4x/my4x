﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// State manager class connecting state machine states with unity. The method delegate in the update of this class is changed by the states.
/// </summary>
public abstract class sm2_StateMachineDefiner : MonoBehaviour
{
	public sm2_State _entryState;
}
