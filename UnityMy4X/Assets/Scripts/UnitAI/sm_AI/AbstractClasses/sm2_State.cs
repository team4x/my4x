using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class sm2_State
{
	/// <summary>
	/// the state manager of the state machine
	/// </summary>
	protected sm2_UnitComponent _unitComponent;

	/// <summary>
	/// the last active state
	/// </summary>
	public sm2_State _lastState;

	/// <summary>
	/// the superstate of this state if existing (if not == null)
	/// </summary>
	protected sm2_State _superState;

	/// <summary>
	/// the active substate if the state has substates. for initialization this has to be set to the entry substate
	/// </summary>
	protected sm2_State _currentSubState;

	protected bool _stateActive; 
	
	
	protected sm2_State(sm2_State superState, sm2_State lastState)
	{
		_superState = superState;
		_lastState = lastState;
	}


	/// <summary>
	/// the state update of this state. 
	/// </summary>
	public void sm2_StateUpdate(sm2_UnitComponent unitComponent)
	{
		_unitComponent = unitComponent;

		GetCurrentDataFromInfoList();

		CheckIfEntry(unitComponent);

		DoTransferOrGoDeeper(unitComponent);
	}

	private void GetCurrentDataFromInfoList()
	{
		throw new NotImplementedException();
	}

	private void CheckIfEntry(sm2_UnitComponent unitComponent)
	{
		//Check if it is entry into state.
		if (_stateActive == false)
		{
			Entry(unitComponent);
			//if the state is entered the first time every entry method of every substate has to be called before a transition happens
			if (_currentSubState != null)
			{
				_currentSubState.CheckIfEntry(unitComponent);
			}
		}
	}

	/// <summary>
	/// If there is a transition, all exits and entries corresponding to the transition are called and then the update of the target state. The state activity and the substates are not entered if there is a transition.
	/// If there is no transition, first the state activity is done and then the current substate is entered.
	/// </summary>
	private void DoTransferOrGoDeeper(sm2_UnitComponent unitComponent)
	{
		//The resulting transfer state from events/transitions from this state into another state
		sm2_State stateFromTransitions = Transitions();

		//there is a transfer
		if (stateFromTransitions != null)
		{
			//performing exits and entries
			TransferToState(stateFromTransitions);
			//calling update of the target state
			stateFromTransitions.sm2_StateUpdate(unitComponent);
		}
		//No transfer to be done. then go on with normal procedure
		else
		{
			StateActivity(); //Stuff the state is doing every update if not transfering

			//i am superstate? if yes, go to current substate update.
			if (_currentSubState != null)
			{
				_currentSubState.sm2_StateUpdate(unitComponent);
			}
		}
	}

	/// <summary>
	/// Method handling the transfer from this state to the transfer state. Calls all necessary exit methods and sets all necessary last state fields in other states.
	/// </summary>
	/// <param name="targetState">
	/// the state to transfer to
	/// </param>
	protected void TransferToState(sm2_State targetState)
	{
		sm2_State highestExitState = null;

		if (targetState._superState == null) //each state has either a superstate or a state manager
		{
			//configurate update chain of state machine
			targetState._unitComponent._currentTopLevelStateUpdate = targetState.sm2_StateUpdate;

			//initialize target state
			targetState._lastState = this;
			targetState.Entry(_unitComponent);

			/*Get the highest exit state from the perspective of this state with the argument that there are no
			 * superstates for the transferState
			 */
			highestExitState =  GetHighestExitState(null);
		}
		else
		{
			//create a list of all superstates of the target state with the target state at index 0
			List<sm2_State> superStatesOfTargetState = new List<sm2_State>();
			superStatesOfTargetState.Add(targetState);
			superStatesOfTargetState.Add(targetState._superState);

			//set last state
			targetState._lastState = this;


			//configurate update chain of state machine including setting last states and calling entries.
			//start with index 1
			int i = 1;
			
			while (superStatesOfTargetState[i] != null) //is true for the first loop
			{
				superStatesOfTargetState[i]._currentSubState = superStatesOfTargetState[i-1];

				if (superStatesOfTargetState[i]._stateActive == false)
				{
					superStatesOfTargetState[i]._lastState = this;
				}

				if (superStatesOfTargetState[i]._superState == null) //true: superstate chain ends here
				{
					superStatesOfTargetState[i]._unitComponent._currentTopLevelStateUpdate = superStatesOfTargetState[i].sm2_StateUpdate;
					break;
				}
				else //there is another superstate and all has to be redone
				{
					superStatesOfTargetState.Add(superStatesOfTargetState[i]._superState);
					i++;
				}
			}

			highestExitState = GetHighestExitState(superStatesOfTargetState);
		}

		//do the exit for all affected states
		if (highestExitState != null)
		{
			ExitStateAndSubStates(highestExitState);
		}
		else
		{
			throw new Exception("No exit state while transfering!!");
		}

	}


	private sm2_State GetHighestExitState(List<sm2_State> superStatesOfTargetState)
	{
		//this state is at least the highest exit state
		sm2_State highestExitState = this;
		
		//creating a list of superstates of this state
		List<sm2_State> superStatesOfThisState = new List<sm2_State>();
		superStatesOfThisState.Add(this); //adding this state at list 0. so index 0 is no superstate.

		if (superStatesOfTargetState == null) //if there are no super states of the target state the highest state of this state is the highest exit state
		{
			int j = 0;
			//get highest superstate
			do
			{
				if (superStatesOfThisState[j]._superState != null)
				{
					superStatesOfThisState.Add(_superState);
					j++;
				}
				else
				{
					highestExitState = superStatesOfThisState[j];
					break;
				}

				if (j > 1000)
				{
					throw new Exception("Hierarchy max reached or problem with superstate chain!");
				}
			} while (true);
		}
		else //target state has superstates
		{
			superStatesOfThisState.Add(this._superState);

			int i = 1;
			//get the last super state not contained in superstates of target
			//initializing conditions for the while loop
			bool superStateIsContained = superStatesOfTargetState.Contains(superStatesOfThisState[i]);
			while (!superStateIsContained) //if superstate is not a s superstate of target state go higher if possible
			{
				if (superStatesOfThisState[i]._superState != null)
				{
					superStatesOfTargetState.Add(superStatesOfThisState[i]._superState);
					i++;
					superStateIsContained = superStatesOfTargetState.Contains(superStatesOfThisState[i]);
				}
				else
				{ 
					break; //stop if there are no superstates anymore
				}

				if (i > 1000)
				{

					throw new Exception("Maximum depth of 1000 achieved!!");
				}
			}

			highestExitState = superStatesOfThisState[i]; //this is after the while loop the highest exit state

		}
	
		return highestExitState;
	}

	/// <summary>
	/// Calls all exit methods for this state and the corresponding substates in the right order.
	/// </summary>
	/// <param name="state"></param>
	protected void ExitStateAndSubStates(sm2_State state)
	{	
		List<sm2_State> listSubStates = new List<sm2_State>();

		listSubStates.Add(state);

		int i = 0;

		while (listSubStates[i]._currentSubState != null)
		{
			listSubStates.Add(listSubStates[i]._currentSubState);
			i++;
			if (i > 1000)
			{
				throw new Exception("Maximum depth of 1000 achieved!!");
			}
		}

		for (int j = listSubStates.Count - 1; j >= 0; j--)
		{
			listSubStates[j].Exit(_unitComponent);
		}
	}

	/// <summary>
	/// Method for recurrent activity when this state is active.
	/// </summary>
	protected abstract void StateActivity();

	/// <summary>
	/// All events and transitions are handled by this method. Only one transfer state can result. This state has to be instantiated if it is not already instantiated.
	/// </summary>
	/// <returns>
	/// the state to transfer to
	/// </returns>
	protected abstract sm2_State Transitions();
	
	/// <summary>
	/// Method for special entry actions for this state.
	/// </summary>
	protected virtual void Entry(sm2_UnitComponent unitComponent)
	{
		_stateActive = true;
	}

	/// <summary>
	/// Method for special exit actions for this state.
	/// </summary>
	protected virtual void Exit(sm2_UnitComponent unitComponent)
	{
		_stateActive = false;
	}
}
