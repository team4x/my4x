using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class sm_State
{
	/// <summary>
	/// switch to save if state is active.
	/// </summary>
	public bool _stateActive = false;

	/// <summary>
	/// the last active state
	/// </summary>
	public sm_State _lastState;

	/// <summary>
	/// the superstate of this state if existing (if not == null)
	/// </summary>
	protected sm_State _superState;

	/// <summary>
	/// the state manager of the state machine if this is a state without superstates
	/// </summary>
	protected sm_StateManager _stateManager;

	/// <summary>
	/// the active substate if the state has substates. for initialization this has to be set to the entry substate
	/// </summary>
	protected sm_State _currentSubState; 
	
	/// <summary>
	/// 
	/// </summary>
	/// <param name="stateManager"></param>
	/// <param name="superState"></param>
	/// <param name="subState"></param>
	protected sm_State(sm_StateManager stateManager, sm_State superState, sm_State subState, sm_State lastState)
	{
		_stateManager = stateManager;
		_superState = superState;
		_currentSubState = subState;
		_lastState = lastState;
	}


	/// <summary>
	/// the state update of this state. 
	/// </summary>
	public void sm_StateUpdate()
	{
		//Check if it is entry into state.
		if (_lastState != this)
		{
			Entry();
			_lastState = this;
		}

		//The resulting transfer state from events/transitions from this state into another state
		sm_State _transferState = Transitions();

		//No transfer to be done. then go on with normal procedure
		if ( _transferState == null)
		{
			//i am superstate? if yes, go to current substate update.
			if (_currentSubState != null)
			{
				_currentSubState.sm_StateUpdate();
			}

			StateActivity(); //Stuff the state is doing every update if not transfering
		}
		//there is a transfer
		else
		{
			TransferToState(_transferState);
		}
	}

	/// <summary>
	/// Method handling the transfer from this state to the transfer state. Calls all necessary exit methods and sets all necessary last state fields in other states.
	/// </summary>
	/// <param name="transferState">
	/// the state to transfer to
	/// </param>
	protected void TransferToState(sm_State transferState)
	{
		sm_State highestExitState = null;

		if (transferState._stateManager != null)
		{
			transferState._stateManager._currentStateUpdate = transferState.sm_StateUpdate;
			transferState._lastState = this;

			highestExitState =  GetHighestExitState(null);
		}
		else
		{
			List<sm_State> superStatesOfTransferState = new List<sm_State>();

			superStatesOfTransferState.Add(transferState);

			superStatesOfTransferState.Add(transferState._superState);

			int i = 1;

			transferState._lastState = this;

			//this has to be true for the very first entry since every state that has not a state manager (thats the case where we are) has at least one superstate
			while (superStatesOfTransferState[i] != null)
			{
				superStatesOfTransferState[i]._currentSubState = superStatesOfTransferState[i-1];

				if (superStatesOfTransferState[i]._stateActive == false)
				{
					superStatesOfTransferState[i]._lastState = this;
				}

				if (superStatesOfTransferState[i]._stateManager != null)
				{
					superStatesOfTransferState[i]._stateManager._currentStateUpdate = superStatesOfTransferState[i].sm_StateUpdate;
					break;
				}
				else 
				{
					superStatesOfTransferState.Add(superStatesOfTransferState[i]._superState);
					i++;
				}
			}

			highestExitState = GetHighestExitState(superStatesOfTransferState);
		}

		if (highestExitState != null)
		{
			ExitStateAndSubStates(highestExitState);
		}

	}


	//TODO: is the original state always exited? YES!
	private sm_State GetHighestExitState(List<sm_State> superStatesOfTransferState)
	{
		sm_State highestExitState = this;

		if (superStatesOfTransferState == null)
		{
			return highestExitState;
		}
		
		List<sm_State> superStatesOfThisState = new List<sm_State>();

		superStatesOfThisState.Add(this);

		int i = 0;
		//first loop sequence is nonsense since highest exit state is at least his state.
		do
		{
			if (!superStatesOfTransferState.Contains(superStatesOfThisState[i]))
			{
				highestExitState = superStatesOfThisState[i];
			}

			if (superStatesOfThisState[i]._superState != null)
			{
				superStatesOfThisState.Add(superStatesOfThisState[i]._superState);
			}
			else
			{
				break;
			}

			i++;

		} while (true);

		return highestExitState;
	}

	/// <summary>
	/// Calls all exit methods for this state and the corresponding substates in the right order.
	/// </summary>
	/// <param name="state"></param>
	protected void ExitStateAndSubStates(sm_State state)
	{
		
		List<sm_State> listSubStates = new List<sm_State>();

		listSubStates.Add(state);

		int i = 0;

		while (listSubStates[i]._currentSubState != null)
		{
			listSubStates.Add(listSubStates[i]._currentSubState);
			i++;
			if (i > 1000)
			{
				throw new Exception("Maximum depth of 1000 achieved!!");
			}
		}

		for (int j = listSubStates.Count - 1; j >= 0; j--)
		{
			listSubStates[j].Exit();
		}
	}

	/// <summary>
	/// Method for recurrent activity when this state is active.
	/// </summary>
	protected abstract void StateActivity();

	/// <summary>
	/// All events and transitions are handled by this method. Only one transfer state can result. This state has to be instantiated if it is not already instantiated.
	/// </summary>
	/// <returns>
	/// the state to transfer to
	/// </returns>
	protected abstract sm_State Transitions();
	
	/// <summary>
	/// Method for special entry actions for this state.
	/// </summary>
	protected virtual void Entry()
	{
		_stateActive = true;
	}

	/// <summary>
	/// Method for special exit actions for this state.
	/// </summary>
	protected virtual void Exit()
	{
		_stateActive = false;
	}
}
