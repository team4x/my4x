using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// the state machine component to be attached to the unit.
/// </summary>
[RequireComponent(typeof(BasicUnit))]
public class sm3_UnitComponent : MonoBehaviour
{
	//a script to define the state by giving the top level state
	[SerializeField]
	protected sm3_StateMachineDefiner _stateMachineDefiner;

	public BasicUnit _thisUnit;

	//this is the method being called on the top level of this state machine
	public delegate void sm3_Update(sm3_UnitComponent unitComponent, bool b1, bool b2);

	public sm3_Update _currentTopLevelStateUpdate;

	/// <summary>
	/// variable storage for the states of the unique state machine
	/// </summary>
	public List<sm3_StateInfo> _infoList = new List<sm3_StateInfo>();

	private void Awake()
	{
		_thisUnit = GetComponent<BasicUnit>();
		_currentTopLevelStateUpdate = _stateMachineDefiner._entryState.StateUpdate;
	}

	void Update()
	{
		if (_currentTopLevelStateUpdate != null)
		{
			_currentTopLevelStateUpdate(this, true , true);
		}
	}


}
