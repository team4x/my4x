using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sm3_DefinerForBasicAI : sm3_StateMachineDefiner
{
	[SerializeField]
	private float _directionHoldTime;
	
	private void Awake()
	{
		_entryState = new sm3_BasicAI(null, _directionHoldTime);
	}
}
