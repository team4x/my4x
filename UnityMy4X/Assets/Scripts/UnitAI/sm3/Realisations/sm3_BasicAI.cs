using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the top level realisiation of a state. The state name is also the name for the state machine.
/// </summary>
public class sm3_BasicAI : sm3_State
{
	//global unit informations
	BasicUnit _thisUnit = null;

	Entity _target = null;

	Destination _destination = null;

	Direction _direction = null;

	//helpers
	float _directionTimer = 0;

	float _directionHoldTime = 0;


	//Substates
	sm3_MoveStates _moveStates = null;

	sm3_TargetHandling _targetHandling = null;

	public sm3_BasicAI(sm3_State superstate, float directionHoldTime) : base(superstate)
	{
		_directionHoldTime = directionHoldTime;
	}

	protected override void EntryActivity()
	{
		_directionTimer = Time.time;

		_direction = new Direction();
	}

	protected override void ExitActivity()
	{
		//none
	}

	protected override void GetInstanceSpecificData(sm3_StateInfo stateInfo)
	{
		sm3_BasicAIInfo basicAIInfo = stateInfo as sm3_BasicAIInfo;

		_thisUnit = basicAIInfo._thisUnit;

		_target = basicAIInfo._target;

		_destination = basicAIInfo._destination;

		_direction = basicAIInfo._direction;

		_directionHoldTime = basicAIInfo._directionHoldTime;

		_directionTimer = basicAIInfo._directionTimer;

		_moveStates = basicAIInfo._moveStates;

		_targetHandling = basicAIInfo._targetHandling;
	}

	protected override void SendInstanceSpecificData(sm3_StateInfo stateInfo)
	{
		sm3_BasicAIInfo basicAIInfo = stateInfo as sm3_BasicAIInfo;

		basicAIInfo._thisUnit = _thisUnit;

		basicAIInfo._target = _target;

		basicAIInfo._destination = _destination;

		basicAIInfo._direction = _direction;

		basicAIInfo._directionHoldTime = _directionHoldTime;

		basicAIInfo._directionTimer = _directionTimer;

		basicAIInfo._moveStates = _moveStates;

		basicAIInfo._targetHandling = _targetHandling;
	}

	protected override void SetEntrySubstate()
	{
		SetSubstateToMoveStates();
	}

	private void SetSubstateToMoveStates()
	{
		if (_moveStates != null)
		{
			_activeSubstate = _moveStates;
		}
		else
		{
			_moveStates = new sm3_MoveStates(this);
			_activeSubstate = _moveStates;
		}
	}

	protected override bool TransitionCheck(out sm3_State targetState, out bool internalTransition)
	{
		internalTransition = true;
		if (NewTarget())
		{
			if (_moveStates != null)
			{
				targetState = _moveStates;
			}
			else
			{
				_moveStates = new sm3_MoveStates(this);
				targetState = _moveStates;
			}

			return true;
		}

		targetState = null;
		return false;
	}

	/// <summary>
	/// returns true if there is a new target. Changes the global target to the new target if there is a new target.
	/// </summary>
	/// <returns></returns>
	private bool NewTarget()
	{
		Entity newTarget = null;
		if ( NewTargetInRange(out newTarget))
		{
			_target = newTarget;
			return true;
		}
		else
		{
			return false;
		}
	}

	protected override void StateActivity()
	{
		if (Time.time - _directionTimer > _directionHoldTime)
		{
			NewRandomDirection();
			_directionTimer = Time.time + (Time.time - _directionHoldTime);
		}
	}

	//TODO: Build priorities for targets into the game. This could mean that not the closest target is being returned.
	/// <summary>
	/// Finds the target closest to this unit.
	/// </summary>
	/// <param name="closestTarget">
	/// gives the closest target. returns null if there is none.
	/// </param>
	/// <returns>
	/// returns true if closest target is not the current target.
	/// </returns>
	private bool NewTargetInRange(out Entity closestTarget)
	{
		//we start like this
		closestTarget = null;
		
		//Do everything only up to the distance of the current target.
		if (_target != null)
		{
			float maxRangeFromCurrentTarget = (_target.transform.position - _thisUnit.transform.position).magnitude;

			//is the current target even further away than the attack range of this unit?
			if (maxRangeFromCurrentTarget < _thisUnit.AttackRange)
			{
				Entity[] entityArray = _thisUnit.TargetsInSpecificRange(maxRangeFromCurrentTarget, true);

				float minSqrDistance = Mathf.Sqrt(float.MaxValue);

				for (int i = 0; i < entityArray.Length; i++)
				{
					float currentSqrDistance = (entityArray[i].transform.position - _thisUnit.transform.position).sqrMagnitude;

					if (currentSqrDistance < minSqrDistance)
					{
						closestTarget = entityArray[i];
						minSqrDistance = currentSqrDistance;
					}
				}
			}
			//if target is further away than engage range just get the closest target
			else
			{
				closestTarget = GetClosestTargetInEngageRange();
			}
		}
		//if there is no target get the closest target
		else
		{
			closestTarget = GetClosestTargetInEngageRange();
		}

		//evaluate if target is new
		if (_target != closestTarget)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/// <summary>
	/// Returns the closest target in the engage range of this unit. Returns null if there is none.
	/// </summary>
	/// <returns></returns>
	private Entity GetClosestTargetInEngageRange()
	{
		Entity returnTarget = null;

		Entity[] entityArray = _thisUnit.TargetsInEngageRange();

		float minSqrDistance = Mathf.Sqrt(float.MaxValue);

		for (int i = 0; i < entityArray.Length; i++)
		{
			float currentSqrDistance = (entityArray[i].transform.position - _thisUnit.transform.position).sqrMagnitude;

			if (currentSqrDistance < minSqrDistance)
			{
				returnTarget = entityArray[i];
				minSqrDistance = currentSqrDistance;
			}
		}

		return returnTarget;
	}

	/// <summary>
	/// Changes the global direction to a random direction in the x-z-plane.
	/// </summary>
	private void NewRandomDirection()
	{
		Vector3 v = new Vector3();

		float r = UnityEngine.Random.Range(0, 2 * Mathf.PI);

		v.y = 0;

		v.x = Mathf.Cos(r);

		v.z = Mathf.Sin(r);

		_direction._direction = v;
	}
}
