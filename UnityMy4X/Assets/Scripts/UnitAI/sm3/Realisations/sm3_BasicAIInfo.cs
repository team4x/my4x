using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sm3_BasicAIInfo : sm3_StateInfo
{
	//global information about unit
	public BasicUnit _thisUnit = null;

	public Entity _target = null;

	public Destination _destination = null;

	public Direction _direction = null;

	//helpers
	public float _directionHoldTime = 0;

	public float _directionTimer = 0;

	//substates
	public sm3_MoveStates _moveStates = null;

	public sm3_TargetHandling _targetHandling = null;


	public sm3_BasicAIInfo(sm3_State stateIdentifier, sm3_State superstate, sm3_State activeSubstate) : base(stateIdentifier, superstate, activeSubstate)
	{
	}
}
