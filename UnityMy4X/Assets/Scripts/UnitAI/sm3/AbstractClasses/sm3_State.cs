using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class sm3_State
{
	/// <summary>
	/// the unique superstate of the state
	/// </summary>
	protected sm3_State _superstate;

	//TODO: needed???
	/// <summary>
	/// superstate should not be changed from extern
	/// </summary>
	sm3_State Superstate { get { return _superstate; } }

	/// <summary>
	/// the current active substate. will be changed often from extern and intern to relink the states.
	/// </summary>
	protected sm3_State _activeSubstate = null;

	public sm3_State(sm3_State superstate)
	{
		_superstate = superstate;
	}

	/// <summary>
	/// This method can be set to doing nothing if there are no substates to be chosen or entry activity.
	/// If only one of the two possibilities is true it can be optimized.
	/// </summary>
	/// <param name="unitComponent"></param>
	protected virtual void Entry(sm3_UnitComponent unitComponent, bool bGetData = true, bool bSendData = true, bool bEntryActivity = true)
	{
		if (bGetData)
		{
			GetData(unitComponent);
		}

		//there is a difference between entering a state and being at the entry point of a state
		if (_activeSubstate == null)
		{
			SetEntrySubstate();
		}
		if (bEntryActivity == true)
		{
			EntryActivity();
		}

		if (bSendData)
		{
			SendData(unitComponent);
		}

	}

	/// <summary>
	/// Everything that has to be done on exit.
	/// </summary>
	/// <param name="unitComponent">
	/// The unit component where data is stored.
	/// </param>
	public virtual void Exit(sm3_UnitComponent unitComponent, bool bGetData = true, bool bSendData = true)
	{
		if (bGetData)
		{
			GetData(unitComponent);
		}
		
		ExitActivity();
		ResetActiveSubstate(unitComponent);

		if (bSendData)
		{
			SendData(unitComponent);
		}
	}

	/// <summary>
	/// sets active substate to null and sends this value to the stateInfo for this state in the unit component 
	/// </summary>
	/// <param name="unitComponent"></param>
	private void ResetActiveSubstate(sm3_UnitComponent unitComponent)
	{
		_activeSubstate = null;
		SendActiveSubstate(unitComponent);
	}

	/// <summary>
	/// gets the stateInfo for this state from the unit component and sends the active substate
	/// </summary>
	/// <param name="unitComponent"></param>
	private void SendActiveSubstate(sm3_UnitComponent unitComponent)
	{
		sm3_StateInfo stateInfo = unitComponent._infoList.Find(x => x._stateIdentifier == this);
		stateInfo._activeSubstate = _activeSubstate;
	}

	/// <summary>
	/// Everything that has to be done if the state is already entered.
	/// </summary>
	/// <param name="unitComponent">
	/// The unit component where data is stored.
	/// </param>
	public void StateUpdate(sm3_UnitComponent unitComponent, bool bGetData = true, bool bSendData = true)
	{
		if (bGetData)
		{
			GetData(unitComponent);
		}
		
		sm3_State targetState;
		bool internalTransition = false;

		if (TransitionCheck(out targetState, out internalTransition))
		{
			if (bSendData)
			{
				SendData(unitComponent);
			}
			
			TransferToState(targetState, unitComponent, internalTransition);
		}
		else
		{
			StateActivity();

			if (bSendData)
			{
				SendData(unitComponent);
			}
			
			GoToSubstateUpdate(unitComponent);
		}
	}

	private void TransferToState(sm3_State targetState, sm3_UnitComponent unitComponent, bool internalTransition)
	{
		//the list of all superstates of this state. these states of to be exited if they are not also sperstates of the target space
		List<sm3_State> exitSuperstateList = new List<sm3_State>();

		int l = 0;

		//this state is the superstate with index 0 of this list. it has to be exited if the transition is not internal
		exitSuperstateList.Add(this);

		//while loop to add all the superstates to the list
		while (exitSuperstateList[l]._superstate != null)
		{
			exitSuperstateList.Add(exitSuperstateList[l]._superstate);
			l++;
		}

		//if it is an internal transition this state has to be a part of the exit list to be remove when checked against the superstates of the target state, i.e. remove the state if it is NOT an internal transition
		if (!internalTransition)
		{
			exitSuperstateList.RemoveAt(0);
		}

		//the list of all superstates of the target state including the target state.
		List<sm3_State> entrySuperstateList = new List<sm3_State>();

		int m = 0;

		//at index 0 the target state is added to the list
		entrySuperstateList.Add(this);

		//loop to add superstates of the target state to the list. checks if target superstates are already in the exit superstates list
		//breaks the loop if necessary and stops it if such a state is found.
		while (true)
		{
			//this is the part to check if the superstate of the target state is already a superstate of the exit state
			if (exitSuperstateList.Contains(entrySuperstateList[m]))
			{
				int cutOff = exitSuperstateList.IndexOf(entrySuperstateList[m]);

				int end = exitSuperstateList.Count - 1;

				//all the superstates including the one found and above have to be removed from the exit list
				for (int n = end; n >= cutOff; n--)
				{
					exitSuperstateList.RemoveAt(n);
				}

				entrySuperstateList.RemoveAt(m);
				break;
			}

			//this is the part where the superstates are added to the list if the condition before this did not break the while loop
			if (entrySuperstateList[m]._superstate != null)
			{
				entrySuperstateList.Add(entrySuperstateList[m]._superstate);
				m++;
			}
			else
			{
				break;
			}

			//exception to avoid infinte loop
			if (m > 1000)
			{
				throw new Exception("Something wrong with the superstates of the target state. There are more than 1K superstates of the target state.");
			}
		}

		//complete the exit list by adding the substates of this state.
		List<sm3_State> exitStateList = new List<sm3_State>();

		exitStateList.Add(this);

		int r = 0;
		while (exitStateList[r]._activeSubstate != null)
		{
			exitStateList.Add(exitStateList[r]._activeSubstate);
			r++;

			if (r > 1000)
			{
				throw new Exception("to many substates for the while loop");
			}
		}

		//remove this state from the list. now only real substates are in the list
		exitStateList.RemoveAt(0);
		//reverse the order so that the deepest substate is at index 0
		exitStateList.Reverse();

		//add all the exit superstates to the list.
		for (int q = 0; q < exitSuperstateList.Count; q++)
		{
			exitStateList.Add(exitSuperstateList[q]);
		}

		sm3_State highestTargetState = null;

		if (entrySuperstateList.Count >= 0)
		{
			highestTargetState = entrySuperstateList[entrySuperstateList.Count - 1];
		}
		else //if this is the case the whole transition is an internal transition with itself as the target state, i.e. there is no entry to do BUT the state machine restarts at the entry point of this state.
		{
			targetState.Entry(unitComponent, true, true, false); //Set entry state without doing entry activity
			targetState.StateUpdate(unitComponent);

			return;
		}

		sm3_State stateToEnter = highestTargetState;

		while (stateToEnter != null)
		{
			stateToEnter.Entry(unitComponent);
			stateToEnter = stateToEnter._activeSubstate;
		}

		highestTargetState.StateUpdate(unitComponent);
	}

	private void GoToSubstateUpdate(sm3_UnitComponent unitComponent)
	{
		_activeSubstate.StateUpdate(unitComponent);
	}

	
	/// <summary>
	/// Gets all the state data from the unit component
	/// </summary>
	/// <param name="unitComponent"></param>
	private void GetData(sm3_UnitComponent unitComponent)
	{
		sm3_StateInfo stateInfo = unitComponent._infoList.Find(x => x._stateIdentifier == this);

		_activeSubstate = stateInfo._activeSubstate;

		GetInstanceSpecificData(stateInfo);
	}

	/// <summary>
	/// Sends all the state data to the unit component
	/// </summary>
	/// <param name="unitComponent"></param>
	private void SendData(sm3_UnitComponent unitComponent)
	{
		sm3_StateInfo stateInfo = unitComponent._infoList.Find(x => x._superstate == this);

		stateInfo._activeSubstate = _activeSubstate;
		
		SendInstanceSpecificData(stateInfo);
	}


	/// <summary>
	/// Checks for transitions.
	/// </summary>
	/// <param name="targetState">
	/// Gives out the target state if there is a transition to be done.
	/// </param>
	/// <param name="internalTransition">
	/// Gives out true if the transition is meant to be an internal transition.
	/// </param>
	/// <returns>
	/// returns true if there is a transition happening.
	/// </returns>
	protected abstract bool TransitionCheck(out sm3_State targetState, out bool internalTransition);

	/// <summary>
	/// Gets the instance specific data from the unit component
	/// </summary>
	/// <param name="unitComponent"></param>
	protected abstract void GetInstanceSpecificData(sm3_StateInfo stateInfo);

	/// <summary>
	/// Sends the instance specific data to the unit component
	/// </summary>
	/// <param name="unitComponent"></param>
	protected abstract void SendInstanceSpecificData(sm3_StateInfo stateInfo);

	/// <summary>
	/// All activity on exit. Does not set the active substate to null.
	/// </summary>
	protected abstract void ExitActivity();

	/// <summary>
	/// All activity on entry. Does not set the entry substate.
	/// </summary>
	protected abstract void EntryActivity();

	/// <summary>
	/// All activity of the state that is done while being active. Does not include transitions.
	/// </summary>
	protected abstract void StateActivity();

	/// <summary>
	/// Sets the entry substate. Needs to be set if there are any subsets beeing entered by this state.
	/// </summary>
	protected abstract void SetEntrySubstate();
}
