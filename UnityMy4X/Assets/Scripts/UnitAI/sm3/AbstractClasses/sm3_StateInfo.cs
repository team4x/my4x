﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sm3_StateInfo 
{
	public sm3_State _stateIdentifier = null;

	public sm3_State _superstate = null;

	public sm3_State _activeSubstate = null;

	protected sm3_StateInfo(sm3_State stateIdentifier, sm3_State superstate, sm3_State activeSubstate )
	{
		_stateIdentifier = stateIdentifier;
		_superstate = superstate;
		_activeSubstate = activeSubstate;
	}
}
