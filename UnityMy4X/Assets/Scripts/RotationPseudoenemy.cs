﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationPseudoenemy : MonoBehaviour
{
	[SerializeField]
	protected float _rotationSpeed = 10;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.Rotate(Vector3.up, Time.deltaTime * _rotationSpeed);	
	}
}
